/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.fidelizacion.concepto.boundary.ConceptoManager;
import py.com.fidelizacion.concepto.entity.Concepto;

/**
 *
 * @author ggauto
 */
@Path("concepto")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ConceptoRest {

    // Para logear
    private static final Logger LOGGER = LoggerFactory.getLogger(ConceptoManager.class);
    @Inject
    ConceptoManager conceptoManager; // Inyectar el manager
    @Context
    protected UriInfo uriInfo;

    @GET
    @Path("/")
    public Response listar() throws WebApplicationException {
        List<Concepto> listaConcepto = conceptoManager.getAll();
        if (listaConcepto.isEmpty()) {
            return Response.noContent().build();
        }
        return Response.ok(listaConcepto).build();

    }

    @GET
    @Path("/{pk}")
    public Response obtener(@PathParam("pk") Integer pk) {
        Concepto concepto = conceptoManager.getById(pk);
        if (concepto == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(concepto).build();
    }

    @POST
    @Path("/")
    public Response crear(Concepto concepto) throws WebApplicationException {
        try {
            // Agrega Concepto en la BD
            Concepto concept = conceptoManager.add(concepto);
            UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
                    .getAbsolutePath());
            URI resourceUri = resourcePathBuilder
                    .path(URLEncoder.encode(concept.getIdConcepto().toString(), "UTF-8"))
                    .build();
            return Response.created(resourceUri).build();
        } catch (Exception e) {
            LOGGER.error("Error : ", e);
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    public Response modificar(Concepto cliente) throws WebApplicationException {
        try {
            Concepto cli = conceptoManager.update(cliente);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.error("Error : ", e);
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("/{pk}")
    public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {
        Concepto concepto = conceptoManager.getById(pk);
        conceptoManager.delete(concepto);
        return Response.ok().build();
    }
}
