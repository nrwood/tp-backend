/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.concepto.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import py.com.fidelizacion.puntos.entity.UsoPuntos;

/**
 *
 * @author ggauto
 */
@Entity
@Table(name = "concepto")
@NamedQueries({
    @NamedQuery(name = "Concepto.findAll", query = "SELECT c FROM Concepto c")
    , @NamedQuery(name = "Concepto.findByIdConcepto", query = "SELECT c FROM Concepto c WHERE c.idConcepto = :idConcepto")
    , @NamedQuery(name = "Concepto.findByDescipcionConcepto", query = "SELECT c FROM Concepto c WHERE c.descipcionConcepto = :descipcionConcepto")
    , @NamedQuery(name = "Concepto.findByPuntosRequerdio", query = "SELECT c FROM Concepto c WHERE c.puntosRequerdio = :puntosRequerdio")})
public class Concepto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idConcepto")
    private Integer idConcepto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descipcionConcepto")
    private String descipcionConcepto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "puntosRequerdio")
    private long puntosRequerdio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConcepto")
    private Collection<UsoPuntos> usoPuntosCollection;

    public Concepto() {
    }

    public Concepto(Integer idConcepto) {
        this.idConcepto = idConcepto;
    }

    public Concepto(Integer idConcepto, String descipcionConcepto, long puntosRequerdio) {
        this.idConcepto = idConcepto;
        this.descipcionConcepto = descipcionConcepto;
        this.puntosRequerdio = puntosRequerdio;
    }

    public Integer getIdConcepto() {
        return idConcepto;
    }

    public void setIdConcepto(Integer idConcepto) {
        this.idConcepto = idConcepto;
    }

    public String getDescipcionConcepto() {
        return descipcionConcepto;
    }

    public void setDescipcionConcepto(String descipcionConcepto) {
        this.descipcionConcepto = descipcionConcepto;
    }

    public long getPuntosRequerdio() {
        return puntosRequerdio;
    }

    public void setPuntosRequerdio(long puntosRequerdio) {
        this.puntosRequerdio = puntosRequerdio;
    }

    public Collection<UsoPuntos> getUsoPuntosCollection() {
        return usoPuntosCollection;
    }

    public void setUsoPuntosCollection(Collection<UsoPuntos> usoPuntosCollection) {
        this.usoPuntosCollection = usoPuntosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConcepto != null ? idConcepto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Concepto)) {
            return false;
        }
        Concepto other = (Concepto) object;
        if ((this.idConcepto == null && other.idConcepto != null) || (this.idConcepto != null && !this.idConcepto.equals(other.idConcepto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.fidelizacion.cliente.entity.Concepto[ idConcepto=" + idConcepto + " ]";
    }
    
}
