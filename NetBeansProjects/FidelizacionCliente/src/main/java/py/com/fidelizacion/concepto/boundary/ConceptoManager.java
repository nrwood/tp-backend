/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.concepto.boundary;

import javax.ejb.Stateless;
import py.com.fidelizacion.concepto.entity.Concepto;
import py.com.fidelizacion.dao.GenericImpl;

/**
 *
 * @author ggauto
 */
@Stateless
public class ConceptoManager extends GenericImpl<Concepto, Integer>{
    
}
