/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.cliente.boundary;

import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.fidelizacion.cliente.entity.Cliente;
import py.com.fidelizacion.dao.GenericImpl;

/**
 *
 * @author ggauto
 */
@Stateless
public class ClienteManager extends GenericImpl<Cliente, Integer>{
    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteManager.class);
    
}
