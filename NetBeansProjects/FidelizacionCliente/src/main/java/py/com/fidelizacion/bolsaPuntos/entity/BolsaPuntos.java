/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.bolsaPuntos.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import py.com.fidelizacion.cliente.entity.Cliente;
import py.com.fidelizacion.puntos.entity.UsoPuntosDetalle;

/**
 *
 * @author ggauto
 */
@Entity
@Table(name = "bolsaPuntos")
@NamedQueries({
    @NamedQuery(name = "BolsaPuntos.findAll", query = "SELECT b FROM BolsaPuntos b")
    , @NamedQuery(name = "BolsaPuntos.findByIdBolsaPunto", query = "SELECT b FROM BolsaPuntos b WHERE b.idBolsaPunto = :idBolsaPunto")
    , @NamedQuery(name = "BolsaPuntos.findByFechaAsignacionPuntaje", query = "SELECT b FROM BolsaPuntos b WHERE b.fechaAsignacionPuntaje = :fechaAsignacionPuntaje")
    , @NamedQuery(name = "BolsaPuntos.findByFechaCaducidadPuntaje", query = "SELECT b FROM BolsaPuntos b WHERE b.fechaCaducidadPuntaje = :fechaCaducidadPuntaje")
    , @NamedQuery(name = "BolsaPuntos.findByPuntajeAsignado", query = "SELECT b FROM BolsaPuntos b WHERE b.puntajeAsignado = :puntajeAsignado")
    , @NamedQuery(name = "BolsaPuntos.findByPuntajeUtilizado", query = "SELECT b FROM BolsaPuntos b WHERE b.puntajeUtilizado = :puntajeUtilizado")
    , @NamedQuery(name = "BolsaPuntos.findBySaldoPuntos", query = "SELECT b FROM BolsaPuntos b WHERE b.saldoPuntos = :saldoPuntos")
    , @NamedQuery(name = "BolsaPuntos.findByMontoOperacion", query = "SELECT b FROM BolsaPuntos b WHERE b.montoOperacion = :montoOperacion")})
public class BolsaPuntos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idBolsaPunto")
    private Integer idBolsaPunto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaAsignacionPuntaje")
    @Temporal(TemporalType.DATE)
    private Date fechaAsignacionPuntaje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaCaducidadPuntaje")
    @Temporal(TemporalType.DATE)
    private Date fechaCaducidadPuntaje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "puntajeAsignado")
    private int puntajeAsignado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "puntajeUtilizado")
    private int puntajeUtilizado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldoPuntos")
    private int saldoPuntos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoOperacion")
    private int montoOperacion;
    @JoinColumn(name = "idCliente", referencedColumnName = "idCliente")
    @ManyToOne(optional = false)
    private Cliente idCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idBolsaPuntos")
    private Collection<UsoPuntosDetalle> usoPuntosDetalleCollection;

    public BolsaPuntos() {
    }

    public BolsaPuntos(Integer idBolsaPunto) {
        this.idBolsaPunto = idBolsaPunto;
    }

    public BolsaPuntos(Integer idBolsaPunto, Date fechaAsignacionPuntaje, Date fechaCaducidadPuntaje, int puntajeAsignado, int puntajeUtilizado, int saldoPuntos, int montoOperacion) {
        this.idBolsaPunto = idBolsaPunto;
        this.fechaAsignacionPuntaje = fechaAsignacionPuntaje;
        this.fechaCaducidadPuntaje = fechaCaducidadPuntaje;
        this.puntajeAsignado = puntajeAsignado;
        this.puntajeUtilizado = puntajeUtilizado;
        this.saldoPuntos = saldoPuntos;
        this.montoOperacion = montoOperacion;
    }

    public Integer getIdBolsaPunto() {
        return idBolsaPunto;
    }

    public void setIdBolsaPunto(Integer idBolsaPunto) {
        this.idBolsaPunto = idBolsaPunto;
    }

    public Date getFechaAsignacionPuntaje() {
        return fechaAsignacionPuntaje;
    }

    public void setFechaAsignacionPuntaje(Date fechaAsignacionPuntaje) {
        this.fechaAsignacionPuntaje = fechaAsignacionPuntaje;
    }

    public Date getFechaCaducidadPuntaje() {
        return fechaCaducidadPuntaje;
    }

    public void setFechaCaducidadPuntaje(Date fechaCaducidadPuntaje) {
        this.fechaCaducidadPuntaje = fechaCaducidadPuntaje;
    }

    public int getPuntajeAsignado() {
        return puntajeAsignado;
    }

    public void setPuntajeAsignado(int puntajeAsignado) {
        this.puntajeAsignado = puntajeAsignado;
    }

    public int getPuntajeUtilizado() {
        return puntajeUtilizado;
    }

    public void setPuntajeUtilizado(int puntajeUtilizado) {
        this.puntajeUtilizado = puntajeUtilizado;
    }

    public int getSaldoPuntos() {
        return saldoPuntos;
    }

    public void setSaldoPuntos(int saldoPuntos) {
        this.saldoPuntos = saldoPuntos;
    }

    public int getMontoOperacion() {
        return montoOperacion;
    }

    public void setMontoOperacion(int montoOperacion) {
        this.montoOperacion = montoOperacion;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public Collection<UsoPuntosDetalle> getUsoPuntosDetalleCollection() {
        return usoPuntosDetalleCollection;
    }

    public void setUsoPuntosDetalleCollection(Collection<UsoPuntosDetalle> usoPuntosDetalleCollection) {
        this.usoPuntosDetalleCollection = usoPuntosDetalleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBolsaPunto != null ? idBolsaPunto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BolsaPuntos)) {
            return false;
        }
        BolsaPuntos other = (BolsaPuntos) object;
        if ((this.idBolsaPunto == null && other.idBolsaPunto != null) || (this.idBolsaPunto != null && !this.idBolsaPunto.equals(other.idBolsaPunto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.fidelizacion.cliente.entity.BolsaPuntos[ idBolsaPunto=" + idBolsaPunto + " ]";
    }
    
}
