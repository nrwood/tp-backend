/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.puntos.entity;

import py.com.fidelizacion.bolsaPuntos.entity.BolsaPuntos;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ggauto
 */
@Entity
@Table(name = "usoPuntosDetalle")
@NamedQueries({
    @NamedQuery(name = "UsoPuntosDetalle.findAll", query = "SELECT u FROM UsoPuntosDetalle u")
    , @NamedQuery(name = "UsoPuntosDetalle.findByIdUsoPuntosDetalle", query = "SELECT u FROM UsoPuntosDetalle u WHERE u.idUsoPuntosDetalle = :idUsoPuntosDetalle")
    , @NamedQuery(name = "UsoPuntosDetalle.findByPuntajeUtilizado", query = "SELECT u FROM UsoPuntosDetalle u WHERE u.puntajeUtilizado = :puntajeUtilizado")})
public class UsoPuntosDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUsoPuntosDetalle")
    private Integer idUsoPuntosDetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "puntajeUtilizado")
    private int puntajeUtilizado;
    @JoinColumn(name = "idBolsaPuntos", referencedColumnName = "idBolsaPunto")
    @ManyToOne(optional = false)
    private BolsaPuntos idBolsaPuntos;
    @JoinColumn(name = "idUsoPuntos", referencedColumnName = "idUsoPuntos")
    @ManyToOne(optional = false)
    private UsoPuntos idUsoPuntos;

    public UsoPuntosDetalle() {
    }

    public UsoPuntosDetalle(Integer idUsoPuntosDetalle) {
        this.idUsoPuntosDetalle = idUsoPuntosDetalle;
    }

    public UsoPuntosDetalle(Integer idUsoPuntosDetalle, int puntajeUtilizado) {
        this.idUsoPuntosDetalle = idUsoPuntosDetalle;
        this.puntajeUtilizado = puntajeUtilizado;
    }

    public Integer getIdUsoPuntosDetalle() {
        return idUsoPuntosDetalle;
    }

    public void setIdUsoPuntosDetalle(Integer idUsoPuntosDetalle) {
        this.idUsoPuntosDetalle = idUsoPuntosDetalle;
    }

    public int getPuntajeUtilizado() {
        return puntajeUtilizado;
    }

    public void setPuntajeUtilizado(int puntajeUtilizado) {
        this.puntajeUtilizado = puntajeUtilizado;
    }

    public BolsaPuntos getIdBolsaPuntos() {
        return idBolsaPuntos;
    }

    public void setIdBolsaPuntos(BolsaPuntos idBolsaPuntos) {
        this.idBolsaPuntos = idBolsaPuntos;
    }

    public UsoPuntos getIdUsoPuntos() {
        return idUsoPuntos;
    }

    public void setIdUsoPuntos(UsoPuntos idUsoPuntos) {
        this.idUsoPuntos = idUsoPuntos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsoPuntosDetalle != null ? idUsoPuntosDetalle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoPuntosDetalle)) {
            return false;
        }
        UsoPuntosDetalle other = (UsoPuntosDetalle) object;
        if ((this.idUsoPuntosDetalle == null && other.idUsoPuntosDetalle != null) || (this.idUsoPuntosDetalle != null && !this.idUsoPuntosDetalle.equals(other.idUsoPuntosDetalle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.fidelizacion.cliente.entity.UsoPuntosDetalle[ idUsoPuntosDetalle=" + idUsoPuntosDetalle + " ]";
    }
    
}
