/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.reglas.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ggauto
 */
@Entity
@Table(name = "reglaAsignacion")
@NamedQueries({
    @NamedQuery(name = "ReglaAsignacion.findAll", query = "SELECT r FROM ReglaAsignacion r")
    , @NamedQuery(name = "ReglaAsignacion.findByIdReglaAsignacion", query = "SELECT r FROM ReglaAsignacion r WHERE r.idReglaAsignacion = :idReglaAsignacion")
    , @NamedQuery(name = "ReglaAsignacion.findByLimiteInferior", query = "SELECT r FROM ReglaAsignacion r WHERE r.limiteInferior = :limiteInferior")
    , @NamedQuery(name = "ReglaAsignacion.findByLimiteSuperior", query = "SELECT r FROM ReglaAsignacion r WHERE r.limiteSuperior = :limiteSuperior")
    , @NamedQuery(name = "ReglaAsignacion.findByMontoEquivalencia", query = "SELECT r FROM ReglaAsignacion r WHERE r.montoEquivalencia = :montoEquivalencia")})
public class ReglaAsignacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idReglaAsignacion")
    private Integer idReglaAsignacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "limiteInferior")
    private int limiteInferior;
    @Basic(optional = false)
    @NotNull
    @Column(name = "limiteSuperior")
    private int limiteSuperior;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoEquivalencia")
    private int montoEquivalencia;

    public ReglaAsignacion() {
    }

    public ReglaAsignacion(Integer idReglaAsignacion) {
        this.idReglaAsignacion = idReglaAsignacion;
    }

    public ReglaAsignacion(Integer idReglaAsignacion, int limiteInferior, int limiteSuperior, int montoEquivalencia) {
        this.idReglaAsignacion = idReglaAsignacion;
        this.limiteInferior = limiteInferior;
        this.limiteSuperior = limiteSuperior;
        this.montoEquivalencia = montoEquivalencia;
    }

    public Integer getIdReglaAsignacion() {
        return idReglaAsignacion;
    }

    public void setIdReglaAsignacion(Integer idReglaAsignacion) {
        this.idReglaAsignacion = idReglaAsignacion;
    }

    public int getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(int limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public int getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(int limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public int getMontoEquivalencia() {
        return montoEquivalencia;
    }

    public void setMontoEquivalencia(int montoEquivalencia) {
        this.montoEquivalencia = montoEquivalencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReglaAsignacion != null ? idReglaAsignacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReglaAsignacion)) {
            return false;
        }
        ReglaAsignacion other = (ReglaAsignacion) object;
        if ((this.idReglaAsignacion == null && other.idReglaAsignacion != null) || (this.idReglaAsignacion != null && !this.idReglaAsignacion.equals(other.idReglaAsignacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.fidelizacion.cliente.entity.ReglaAsignacion[ idReglaAsignacion=" + idReglaAsignacion + " ]";
    }
    
}
