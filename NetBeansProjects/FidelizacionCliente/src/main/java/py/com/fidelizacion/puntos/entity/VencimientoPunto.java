/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.puntos.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ggauto
 */
@Entity
@Table(name = "vencimientoPunto")
@NamedQueries({
    @NamedQuery(name = "VencimientoPunto.findAll", query = "SELECT v FROM VencimientoPunto v")
    , @NamedQuery(name = "VencimientoPunto.findByIdVencimientoPunto", query = "SELECT v FROM VencimientoPunto v WHERE v.idVencimientoPunto = :idVencimientoPunto")
    , @NamedQuery(name = "VencimientoPunto.findByFechaInicioValidez", query = "SELECT v FROM VencimientoPunto v WHERE v.fechaInicioValidez = :fechaInicioValidez")
    , @NamedQuery(name = "VencimientoPunto.findByFechaFinValidez", query = "SELECT v FROM VencimientoPunto v WHERE v.fechaFinValidez = :fechaFinValidez")
    , @NamedQuery(name = "VencimientoPunto.findByDiasDuracionPuntaje", query = "SELECT v FROM VencimientoPunto v WHERE v.diasDuracionPuntaje = :diasDuracionPuntaje")})
public class VencimientoPunto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idVencimientoPunto")
    private Integer idVencimientoPunto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaInicioValidez")
    @Temporal(TemporalType.DATE)
    private Date fechaInicioValidez;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaFinValidez")
    @Temporal(TemporalType.DATE)
    private Date fechaFinValidez;
    @Basic(optional = false)
    @NotNull
    @Column(name = "diasDuracionPuntaje")
    private int diasDuracionPuntaje;

    public VencimientoPunto() {
    }

    public VencimientoPunto(Integer idVencimientoPunto) {
        this.idVencimientoPunto = idVencimientoPunto;
    }

    public VencimientoPunto(Integer idVencimientoPunto, Date fechaInicioValidez, Date fechaFinValidez, int diasDuracionPuntaje) {
        this.idVencimientoPunto = idVencimientoPunto;
        this.fechaInicioValidez = fechaInicioValidez;
        this.fechaFinValidez = fechaFinValidez;
        this.diasDuracionPuntaje = diasDuracionPuntaje;
    }

    public Integer getIdVencimientoPunto() {
        return idVencimientoPunto;
    }

    public void setIdVencimientoPunto(Integer idVencimientoPunto) {
        this.idVencimientoPunto = idVencimientoPunto;
    }

    public Date getFechaInicioValidez() {
        return fechaInicioValidez;
    }

    public void setFechaInicioValidez(Date fechaInicioValidez) {
        this.fechaInicioValidez = fechaInicioValidez;
    }

    public Date getFechaFinValidez() {
        return fechaFinValidez;
    }

    public void setFechaFinValidez(Date fechaFinValidez) {
        this.fechaFinValidez = fechaFinValidez;
    }

    public int getDiasDuracionPuntaje() {
        return diasDuracionPuntaje;
    }

    public void setDiasDuracionPuntaje(int diasDuracionPuntaje) {
        this.diasDuracionPuntaje = diasDuracionPuntaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVencimientoPunto != null ? idVencimientoPunto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VencimientoPunto)) {
            return false;
        }
        VencimientoPunto other = (VencimientoPunto) object;
        if ((this.idVencimientoPunto == null && other.idVencimientoPunto != null) || (this.idVencimientoPunto != null && !this.idVencimientoPunto.equals(other.idVencimientoPunto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.fidelizacion.cliente.entity.VencimientoPunto[ idVencimientoPunto=" + idVencimientoPunto + " ]";
    }
    
}
