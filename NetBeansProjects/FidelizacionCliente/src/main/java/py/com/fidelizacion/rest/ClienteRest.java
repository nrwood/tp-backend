/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.fidelizacion.cliente.boundary.ClienteManager;
import py.com.fidelizacion.cliente.entity.Cliente;

/**
 *
 * @author ggauto
 */
@Path("cliente")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ClienteRest {

    // Para logear
    private static final Logger LOGGER = LoggerFactory.getLogger(ClienteManager.class);
    @Inject
    ClienteManager clienteManager; // Inyectar el manager
    @Context
    protected UriInfo uriInfo;

    @GET
    @Path("/")
    public Response listar() throws WebApplicationException {
        List<Cliente> listaCliente = clienteManager.getAll();
        if (listaCliente.isEmpty()) {
            return Response.noContent().build();
        }
        return Response.ok(listaCliente).build();

    }

    @GET
    @Path("/{pk}")
    public Response obtener(@PathParam("pk") Integer pk) {
        Cliente cliente = clienteManager.getById(pk);
        if (cliente == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(cliente).build();
    }

    @POST
    @Path("/")
    public Response crear(Cliente cliente) throws WebApplicationException {
        try {
            // Agrega Cliente en la BD
            Cliente cli = clienteManager.add(cliente);
            UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
                    .getAbsolutePath());
            URI resourceUri = resourcePathBuilder
                    .path(URLEncoder.encode(cli.getIdCliente().toString(), "UTF-8"))
                    .build();
            return Response.created(resourceUri).build();
        } catch (Exception e) {
            LOGGER.error("Error : ", e);
            return Response.serverError().build();
        }
    }

    @PUT
    @Path("/")
    public Response modificar(Cliente cliente) throws WebApplicationException {
        try {
            Cliente cli = clienteManager.update(cliente);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.error("Error : ", e);
            return Response.serverError().build();
        }
    }

    @DELETE
    @Path("/{pk}")
    public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {
        Cliente cliente = clienteManager.getById(pk);
        clienteManager.delete(cliente);
        return Response.ok().build();
    }
}
