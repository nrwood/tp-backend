/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.fidelizacion.puntos.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ggauto
 */
@Entity
@Table(name = "usoPuntos")
@NamedQueries({
    @NamedQuery(name = "UsoPuntos.findAll", query = "SELECT u FROM UsoPuntos u")
    , @NamedQuery(name = "UsoPuntos.findByIdUsoPuntos", query = "SELECT u FROM UsoPuntos u WHERE u.idUsoPuntos = :idUsoPuntos")
    , @NamedQuery(name = "UsoPuntos.findByPuntajeUtilizado", query = "SELECT u FROM UsoPuntos u WHERE u.puntajeUtilizado = :puntajeUtilizado")
    , @NamedQuery(name = "UsoPuntos.findByFecha", query = "SELECT u FROM UsoPuntos u WHERE u.fecha = :fecha")})
public class UsoPuntos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUsoPuntos")
    private Integer idUsoPuntos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "puntajeUtilizado")
    private int puntajeUtilizado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;

    public UsoPuntos() {
    }

    public UsoPuntos(Integer idUsoPuntos) {
        this.idUsoPuntos = idUsoPuntos;
    }

    public UsoPuntos(Integer idUsoPuntos, int puntajeUtilizado, Date fecha) {
        this.idUsoPuntos = idUsoPuntos;
        this.puntajeUtilizado = puntajeUtilizado;
        this.fecha = fecha;
    }

    public Integer getIdUsoPuntos() {
        return idUsoPuntos;
    }

    public void setIdUsoPuntos(Integer idUsoPuntos) {
        this.idUsoPuntos = idUsoPuntos;
    }

    public int getPuntajeUtilizado() {
        return puntajeUtilizado;
    }

    public void setPuntajeUtilizado(int puntajeUtilizado) {
        this.puntajeUtilizado = puntajeUtilizado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsoPuntos != null ? idUsoPuntos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsoPuntos)) {
            return false;
        }
        UsoPuntos other = (UsoPuntos) object;
        if ((this.idUsoPuntos == null && other.idUsoPuntos != null) || (this.idUsoPuntos != null && !this.idUsoPuntos.equals(other.idUsoPuntos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.fidelizacion.puntos.entity.UsoPuntos[ idUsoPuntos=" + idUsoPuntos + " ]";
    }
    
}
